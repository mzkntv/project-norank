/* eslint-disable */
// src/boot/axios.js

import { boot } from 'quasar/wrappers';
import axios from 'axios';
import { Notify } from 'quasar';

const api = axios.create({
  baseURL: process.env.VUE_DOMAIN_NAME,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default boot(({ app, store, router }) => {
  const requestHandler = (request) => {
    const token = localStorage.getItem('access_token') || '';
    if (token) {
      request.headers.Authorization = `Bearer ${token}`;
    }
    return request;
  };

  const errorHandler = error => {
    return Promise.reject(error);
  };

  api.interceptors.request.use(
    (request) => requestHandler(request),
    (error) => errorHandler(error),
  );

  api.interceptors.response.use(
    (response) => response,
    (error) => {
      if (error.response.status !== 401) {
        return Promise.reject(error);
      }
      store.dispatch('auth/logout');
      Notify.create({
        type: 'negative',
        message: 'Упс! Ваш сеанс работы закончился, пожалуйста войдите снова',
      });
      router.push({ name: 'login' });
    },
  );
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api;
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
});

export { axios, api };
