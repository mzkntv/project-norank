const routes = [
  {
    path: '/workspace',
    component: () => import('layouts/AppLayout'),
    meta: { auth: true },
    children: [
      {
        path: '',
        redirect: { name: 'workspace-events-list' },
        component: () => import('pages/Index'),
      },
      {
        path: 'account',
        component: () => import('pages/workspace/AccountPage'),
        children: [
          {
            path: '',
            name: 'workspace-account',
            component: () => import('components/workspace/account/AccountHome'),
          },
        ],
      },
      {
        path: 'manage',
        component: () => import('pages/workspace/ManagePage'),
        children: [
          {
            path: '',
            name: 'workspace-manage-list',
            component: () => import('components/workspace/management/ManageHome'),
          },
          {
            path: ':id',
            name: 'workspace-manage-detail',
            component: () => import('components/workspace/management/MemberEdit'),
            props: (route) => ({ memberId: route.params.id }),
          },
        ],
      },
      {
        path: 'trains',
        component: () => import('pages/workspace/TrainPage'),
        children: [
          {
            path: '',
            name: 'workspace-trains-list',
            component: () => import('components/workspace/trains/TrainHome'),
          },
          {
            path: ':id',
            name: 'workspace-trains-detail',
            component: () => import('components/workspace/trains/TrainDetail'),
          },
        ],
      },
      {
        path: 'events',
        name: 'workspace-events',
        component: () => import('pages/workspace/Events'),
        children: [
          {
            path: '',
            name: 'workspace-events-list',
            component: () => import('components/workspace/events/EventHome'),
          },
          {
            path: ':id',
            name: 'workspace-event-detail',
            component: () => import('components/workspace/events/EventDetail'),
            props: (route) => ({ eventId: Number(route.params.id) }),
          },
        ],
      },
    ],
  },
  {
    path: '/',
    name: 'landing',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        redirect: { name: 'workspace-events-list' },
        component: () => import('pages/Index.vue'),
      },
      { path: 'steam-login', component: () => import('pages/SteamLogin.vue') },
    ],
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('pages/Login'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
