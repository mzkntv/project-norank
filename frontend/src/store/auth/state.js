export default function state() {
  return {
    status: '',
    token: localStorage.getItem('access_token') || '',
  };
}
