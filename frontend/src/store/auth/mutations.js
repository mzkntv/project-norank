export function authRequest(state) {
  state.status = 'loading';
}

export function authSuccess(state, token) {
  state.status = 'success';
  state.token = token;
}

export function authError(state) {
  state.status = 'error';
}

export function authLogout(state) {
  state.status = '';
  state.token = '';
}
