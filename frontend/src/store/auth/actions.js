import { api } from 'boot/axios';
import { URLSearchParams } from 'core-js/modules/web.url-search-params';

export function logout({ commit }) {
  return new Promise((resolve) => {
    commit('authLogout');
    localStorage.removeItem('access_token');
    resolve();
  });
}

export function login({ commit }, user) {
  const params = new URLSearchParams({
    username: user.username,
    password: user.rawPassword,
  }).toString();

  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  };

  return new Promise((resolve, reject) => {
    commit('authRequest');
    api.post('/api/auth/sign-in', params, config)
      .then((response) => {
        const accessToken = response.data.access_token;
        api.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
        localStorage.setItem('access_token', accessToken);
        commit('authSuccess', accessToken);
        resolve(accessToken);
      })
      .catch((error) => {
        commit('authError');
        localStorage.removeItem('access_token');
        reject(error);
      });
  });
}

export function register({ commit }, user) {
  return new Promise((resolve, reject) => {
    commit('authRequest');
    const params = JSON.stringify({
      email: user.mail,
      username: user.username,
      password: user.password,
    });
    api.post('/api/auth/sign-up', params)
      .then((response) => {
        const accessToken = response.data.access_token;
        api.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
        localStorage.setItem('access_token', accessToken);
        commit('authSuccess', accessToken);
        resolve(accessToken);
      })
      .catch((error) => {
        commit('authError');
        localStorage.removeItem('access_token');
        reject(error);
      });
  });
}

export function registerViaSteam({ commit }, requestUrl) {
  return new Promise((resolve, reject) => {
    commit('authRequest');
    api.get(requestUrl)
      .then((response) => {
        const accessToken = response.data.access_token;
        api.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
        localStorage.setItem('access_token', accessToken);
        commit('authSuccess', accessToken);
        resolve(accessToken);
      })
      .catch((error) => {
        commit('authError');
        localStorage.removeItem('access_token');
        reject(error);
      });
  });
}
