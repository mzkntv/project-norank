import jwtDecode from 'jwt-decode';

export function isLoggedIn(state) {
  return !!state.token;
}

export function authScopes(state) {
  try {
    const { scopes } = jwtDecode(state.token);
    return scopes;
  } catch (e) {
    return {};
  }
}
