server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name norank.ru www.norank.ru;

        root /var/www/html;

        charset utf-8;

        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/norank.ru/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/norank.ru/privkey.pem;

        ssl_buffer_size 8k;

        ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

        ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
        ssl_prefer_server_ciphers on;

        ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

        ssl_ecdh_curve secp384r1;
        ssl_session_tickets off;

        ssl_stapling on;
        ssl_stapling_verify on;
        resolver 8.8.8.8;

        add_header X-Frame-Options "SAMEORIGIN" always;
        add_header X-XSS-Protection "1; mode=block" always;
        add_header X-Content-Type-Options "nosniff" always;
        add_header Referrer-Policy "no-referrer-when-downgrade" always;
        add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;

        location / {
            add_header Cache-Control "no-cache, no-store, must-revalidate";
            try_files $uri $uri/ /index.html;
            index index.html;
        }

        location ~* ^.+\.(?:jpg|jpeg|gif|png|svg|webp|js|css|eot|otf|ttf|ttc|woff|woff2|font.css)$ {
            access_log off;
            expires 30d;
            add_header Cache-Control "public, max-age=604800";

            ## No need to bleed constant updates. Send the all shebang in one
            ## fell swoop.
            tcp_nodelay off;

            ## Set the OS file cache.
            open_file_cache max=3000 inactive=120s;
            open_file_cache_valid 45s;
            open_file_cache_min_uses 2;
            open_file_cache_errors off;
        }

        location /api/ {
            proxy_pass http://api/;
        }

        error_page  404 /;

        access_log off;
        error_log  /var/log/nginx/norank.ru-error.log error;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }
}
