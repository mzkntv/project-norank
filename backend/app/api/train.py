from typing import List

from fastapi import APIRouter, status, Depends, Security

from app.schemas.auth import User
from app.schemas.train import Train, TrainCreate, TrainList
from app.services.auth import get_current_active_user
from app.services.train import TrainService

router = APIRouter(
    prefix='/trains',
    tags=['trains'],
    dependencies=[Security(get_current_active_user)]
)


@router.get('/', response_model=List[TrainList])
async def get_trains(
        user: User = Depends(get_current_active_user),
        service: TrainService = Depends(),
):
    return service.get_trains_by_owner(user.id)


@router.get('/{train_id}', response_model=Train)
async def get_train(
    train_id: int,
    service: TrainService = Depends(),
):
    return service.get_train(train_id)


@router.post('/', response_model=Train, status_code=status.HTTP_201_CREATED)
async def create_train(
    train_data: TrainCreate,
    user: User = Depends(get_current_active_user),
    service: TrainService = Depends(),
):
    return service.create_train(train_data, user.id)


@router.delete('/{train_id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_train(
        train_id: int,
        user: User = Depends(get_current_active_user),
        service: TrainService = Depends(),
):
    service.delete_train_by_owner(train_id, user.id)
