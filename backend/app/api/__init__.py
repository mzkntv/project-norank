from fastapi import APIRouter

from . import (
    train,
    depot,
    event,
    auth,
    ban,
    wagon,
    user,
    role,
)

router = APIRouter()
router.include_router(train.router)
router.include_router(depot.router)
router.include_router(event.router)
router.include_router(auth.router)
router.include_router(ban.router)
router.include_router(wagon.router)
router.include_router(user.router)
router.include_router(role.router)
