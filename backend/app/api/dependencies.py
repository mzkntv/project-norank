from typing import Generator

from app.db.session import Session


def get_session() -> Generator:
    """
    Create session to interact with database
    :return: session instance
    """
    session = Session()
    try:
        yield session
    finally:
        session.close()
