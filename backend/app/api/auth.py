"""
Authentication routes to sign-in, sign-up and get user settings
"""
from fastapi import APIRouter, Depends, status, Security
from fastapi.security import OAuth2PasswordRequestForm
from starlette.requests import Request

from app.schemas.auth import UserCreate, Token, User
from app.services.auth import AuthService, get_current_user, SteamAuthService, get_current_active_user

router = APIRouter(
    prefix='/auth',
    tags=['auth']
)


@router.get('/sign-up/steam', response_model=Token)
def proceed_steam_sign_up(request: Request, service: SteamAuthService = Depends()) -> Token:
    """Validate response from Steam provider and redirect user to start"""
    return service.register_new_user(request.query_params)


@router.post('/sign-up', response_model=Token, status_code=status.HTTP_201_CREATED)
def sign_up(user_data: UserCreate, service: AuthService = Depends()):
    """
    Registration route.
    Can register user and gives authorization token after successful registration
    """
    return service.register_new_user(user_data)


@router.get('/sign-in/steam', response_model=Token)
def steam_sign_in(request: Request, service: SteamAuthService = Depends()) -> Token:
    """
    Authentication route via Steam provider.
    Validate response from Steam provider and gives token.
    """
    return service.authenticate_user(request.query_params)


@router.post('/sign-in/gmod', response_model=Token, dependencies=[Security(get_current_active_user, scopes=[])])
def sign_in_by_gaming_server(
        client_steam_id: str,
        service: SteamAuthService = Depends()
) -> Token:
    """
    Authentication via gaming service
    :param client_steam_id: SteamID64 format
    :param service: Service to resolve business logic
    :return: Client's token
    """
    return service.authenticate_via_gaming_server_token(client_steam_id)


@router.post('/sign-in', response_model=Token)
def sign_in(form_data: OAuth2PasswordRequestForm = Depends(), service: AuthService = Depends()):
    """Authentication route. Can give authorization token to user with valid username & password"""
    return service.authenticate_user(form_data.username, form_data.password)


@router.get('/user', response_model=User)
def get_user(user: User = Depends(get_current_user)):
    """Return information about authorized user"""
    return user
