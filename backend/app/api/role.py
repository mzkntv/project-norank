from typing import List

from fastapi import APIRouter, Depends, Security
from starlette import status

from app.schemas.users.role import Role, RoleCreate
from app.services.auth import get_current_active_user
from app.services.user import UserService

router = APIRouter(
    prefix='/roles',
    tags=['users'],
    dependencies=[Security(get_current_active_user, scopes=['role:view'])]
)


@router.get('/', response_model=List[Role])
async def get_all_roles(
        service: UserService = Depends(),
):
    return service.get_list_of_all_roles()


@router.get('/{role_id}/', response_model=Role)
async def get_role_by_role_id(
        role_id: int,
        service: UserService = Depends(),
):
    return service.get_role_info(role_id)


@router.post('/', response_model=Role, status_code=status.HTTP_201_CREATED)
async def create_role(
        role_data: RoleCreate,
        service: UserService = Depends(),
):
    return service.create_new_role(role_data)
