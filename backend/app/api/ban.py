from typing import List

from fastapi import APIRouter, Depends, Security
from starlette import status

from app.models import User
from app.schemas.ban import Ban, BanCreate, BanDelete
from app.services.auth import get_current_active_user
from app.services.ban import BanService

router = APIRouter(
    prefix='/bans',
    tags=['ban'],
    dependencies=[Security(get_current_active_user, scopes=['ban:view'])]
)


@router.get('/', response_model=List[Ban])
def get_bans(
        service: BanService = Depends(),
) -> List[Ban]:
    """Получить историю наказаний"""
    return service.get_many()


@router.post('/', response_model=Ban)
def create_ban(
        perpetrator_data: BanCreate,
        user: User = Security(get_current_active_user, scopes=['ban:create']),
        service: BanService = Depends()
) -> Ban:
    """Забанить пользователя с айди user_id"""
    return service.ban_user(perpetrator_data, user.id)


@router.delete('/', status_code=status.HTTP_204_NO_CONTENT)
def delete_ban(
        ban_data: BanDelete,
        user: User = Security(get_current_active_user, scopes=['ban:update']),
        service: BanService = Depends(),
):
    """Разбанить игрока с айди user_id. Сама запись из БД не удаляется."""
    service.unban_user(ban_data, user.id)
