from typing import List

from fastapi import APIRouter, Depends

from app.schemas.auth import User
from app.schemas.wagons.settings import WagonSetting, WagonSettingCreate
from app.services.auth import get_current_user
from app.services.wagon import WagonService

router = APIRouter(
    prefix='/{wagon_id}/settings',
)


@router.get('/', response_model=List[WagonSetting])
async def get_settings(
        wagon_id: int,
        user: User = Depends(get_current_user),
        service: WagonService = Depends(),
):
    return service.get_wagon_settings(wagon_id)


@router.post('/', response_model=List[WagonSetting])
async def create_settings(
        wagon_id: int,
        settings_data: List[WagonSettingCreate],
        user: User = Depends(get_current_user),
        service: WagonService = Depends(),
):
    return service.add_wagon_settings(wagon_id, settings_data)
