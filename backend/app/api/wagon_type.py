from typing import List

from fastapi import APIRouter, Depends

from app.schemas.auth import User
from app.schemas.wagon import WagonType, BaseWagonType
from app.services.auth import get_current_user
from app.services.wagon import WagonService

router = APIRouter(
    prefix='/types',
    tags=['wagon'],
)


@router.get('/', response_model=List[WagonType])
async def get_types(service: WagonService = Depends()):
    return service.get_all_types()


@router.get('/{type_id}', response_model=WagonType)
async def get_type(
        type_id: int,
        service: WagonService = Depends()
):
    return service.get_wagon_type(type_id)


@router.post('/', response_model=WagonType)
async def create_type(
        wagon_data: BaseWagonType,
        user: User = Depends(get_current_user),
        service: WagonService = Depends(),
):
    return service.create_wagon_type(wagon_data)
