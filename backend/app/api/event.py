from typing import List

from fastapi import APIRouter, status, Depends, Body, Security

from app.schemas.auth import User
from app.schemas.event import (
    EventInstance,
    EventInstanceCreate,
    EventInstanceUpdate,
    MetrostroiEventMember,
    EventInstanceWithUserInfo,
)
from app.schemas.train import Train
from app.services.auth import get_current_active_user
from app.services.event import EventInstanceService
from app.services.train import TrainService

router = APIRouter(
    prefix='/events',
    tags=['events'],
    dependencies=[Security(get_current_active_user)]
)


@router.get('/', response_model=List[EventInstanceWithUserInfo])
async def get_events(
        service: EventInstanceService = Depends(),
) -> List[EventInstance]:
    return service.get_many()


@router.get('/{event_id}', response_model=EventInstanceWithUserInfo)
async def get_event(
    event_id: int,
    user: User = Depends(get_current_active_user),
    service: EventInstanceService = Depends(),
) -> EventInstance:
    return service.get(event_id, user.id)


@router.post('/', response_model=EventInstance, status_code=status.HTTP_201_CREATED)
async def create_event(
    event_data: EventInstanceCreate,
    user: User = Security(get_current_active_user, scopes=['event:create']),
    service: EventInstanceService = Depends(),
):
    return service.create(event_data, user.id)


@router.patch('/{event_id}', response_model=EventInstance)
async def update_event(
    event_id: int,
    event_data: EventInstanceUpdate,
    user: User = Security(get_current_active_user, scopes=['event:update']),
    service: EventInstanceService = Depends(),
):
    return service.update(event_id, user.id, event_data)


@router.delete('/{event_id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_event(
    event_id: int,
    user: User = Security(get_current_active_user, scopes=['event:delete']),
    service: EventInstanceService = Depends(),
):
    return service.delete(event_id, user.id)


@router.post('/{event_id}/register', status_code=status.HTTP_201_CREATED, response_model=MetrostroiEventMember)
def event_register(
    event_id: int,
    train_id: int = Body(..., embed=True),
    user: User = Security(get_current_active_user),
    service: EventInstanceService = Depends(),
):
    return service.subscribe_user_to_event(user.id, event_id, train_id)


@router.delete('/{event_id}/register', status_code=status.HTTP_204_NO_CONTENT)
def event_register(
    event_id: int,
    user: User = Security(get_current_active_user),
    service: EventInstanceService = Depends(),
):
    return service.unsubscribe_user_from_event(user.id, event_id)


@router.get('/{event_id}/trains', response_model=List[Train])
def event_trains(
    event_id: int,
    service: TrainService = Depends(),
):
    return service.get_available_trains(event_id)


@router.get('/{event_id}/participants', response_model=List[User])
def get_participants_of_event(
        event_id: int,
        service: EventInstanceService = Depends(),
):
    return service.get_list_of_participants(event_id)
