from typing import List

from fastapi import APIRouter, Depends

from app.schemas.auth import User
from app.schemas.train import Depot, DepotCreate
from app.services.auth import get_current_user
from app.services.train import DepotService

router = APIRouter(
    prefix='/depot',
    tags=['trains']
)


@router.get('/', response_model=List[Depot])
def get_depots(
    service: DepotService = Depends(),
):
    return service.get_many()


@router.get('/{depot_id}', response_model=Depot)
def get_depot(
    depot_id: int,
    service: DepotService = Depends(),
):
    return service.get(depot_id)


@router.post('/', response_model=Depot)
def create_depot(
    depot_data: DepotCreate,
    user: User = Depends(get_current_user),
    service: DepotService = Depends(),
):
    return service.create_depot(depot_data, user.id)
