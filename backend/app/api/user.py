"""
User's management endpoints
"""
from typing import List

from fastapi import APIRouter, Depends

from app.schemas.auth import User
from app.schemas.users.permission import Permission
from app.schemas.users.role import Role
from app.services.user import UserService

router = APIRouter(
    prefix='/users',
    tags=['users'],
)


@router.get('/', response_model=List[User])
async def get_user_list(
        service: UserService = Depends(),
):
    return service.get_list_of_all_users()


@router.get('/{user_id}/roles', response_model=List[Role])
async def get_user_roles(
        user_id: int,
        service: UserService = Depends(),
):
    return service.get_user_roles(user_id)


@router.get('/{user_id}/permissions', response_model=List[Permission])
async def get_user_permissions(
        user_id: int,
        service: UserService = Depends(),
):
    return service.get_user_permissions(user_id)


@router.get('/{user_id}', response_model=User)
async def get_user(
        user_id: int,
        service: UserService = Depends(),
):
    return service.get_user_info_by_user_id(user_id)
