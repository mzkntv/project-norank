from typing import List

from fastapi import APIRouter, Depends
from starlette import status

from . import wagon_type, wagon_settings
from ..schemas.auth import User
from ..schemas.wagon import Wagon, WagonCreate
from ..services.auth import get_current_user
from ..services.wagon import WagonService

router = APIRouter(
    prefix='/wagon',
    tags=['wagon'],
)

router.include_router(wagon_type.router)
router.include_router(wagon_settings.router)


@router.get('/', response_model=List[Wagon])
async def get_all_wagons(service: WagonService = Depends()):
    return service.get_all_wagons()


@router.get('/{wagon_id}', response_model=Wagon)
async def get_wagon(
        wagon_id: int,
        service: WagonService = Depends()
):
    return service.get_wagon(wagon_id)


@router.post('/', response_model=List[Wagon], status_code=status.HTTP_201_CREATED)
async def create_wagons(
        wagon_data: List[WagonCreate],
        user: User = Depends(get_current_user),
        service: WagonService = Depends(),
):
    return service.create_wagons(user.id, wagon_data)
