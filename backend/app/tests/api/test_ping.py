from fastapi.testclient import TestClient
from fastapi import status


def test_ping(client: TestClient):
    response = client.get('/ping')
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {'ping': 'pong'}
