import pytest
from fastapi.testclient import TestClient
from yarl import URL

from app.main import app
from app.settings import settings


@pytest.fixture(scope='module')
def client():
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope='session')
def pg_url():
    """
    Provides base PostgreSQL URL for creating temporary databases.
    """
    return URL(settings.database_url)
