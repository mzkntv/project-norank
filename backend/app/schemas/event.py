from datetime import datetime
from typing import Optional

from pydantic import BaseModel

from app.schemas.auth import UserInfo


class BaseEventInstance(BaseModel):
    name: str
    description: Optional[str]
    start: Optional[datetime]
    finish: Optional[datetime]
    max_number_of_participants: Optional[int]


class EventInstanceCreate(BaseEventInstance):
    pass


class EventInstanceUpdate(BaseEventInstance):
    pass


class EventInstance(BaseEventInstance):
    id: int
    created_at: datetime
    updated_at: Optional[datetime]
    updated_by: Optional[int]
    participants_count: int

    class Config:
        orm_mode = True


class EventInstanceWithUserInfo(EventInstance):
    created_by: Optional[UserInfo] = {}
    is_registered: Optional[bool] = False


class BaseEventMember(BaseModel):
    user_id: int
    event_id: int


class EventMemberCreate(BaseEventMember):
    pass


class EventMemberUpdate(BaseEventMember):
    pass


class EventMember(BaseEventMember):
    id: int

    class Config:
        orm_mode = True


class BaseMetrostroiEventMember(BaseEventMember):
    train_id: int


class MetrostroiEventMemberCreate(BaseMetrostroiEventMember):
    pass


class MetrostroiEventMemberUpdate(BaseMetrostroiEventMember):
    pass


class MetrostroiEventMember(BaseMetrostroiEventMember):
    id: int

    class Config:
        orm_mode = True
