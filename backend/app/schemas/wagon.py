from datetime import datetime
from typing import Optional, List, Union

from pydantic import BaseModel

from app.schemas.wagons.settings import WagonSettingCreate, WagonSetting


class BaseWagonType(BaseModel):
    """
    Базовая датаграмма типа вагона
    """
    code: str
    metrostroi_representation_class: str
    name: str
    description: Optional[str] = ''


class WagonType(BaseWagonType):
    """
    Датаграмма типа вагона для ответа сервером
    """
    id: int
    created_at: datetime
    updated_at: Optional[datetime]

    class Config:
        orm_mode = True


class WagonTypeShortInfo(BaseModel):
    id: int
    code: str
    name: str
    metrostroi_representation_class: str

    class Config:
        orm_mode = True


class BaseWagon(BaseModel):
    """
    Базовая датаграмма вагона
    """
    registration_number: Optional[int]
    mileage: int
    train_position: int
    train_id: int
    inverted: bool


class WagonCreate(BaseWagon):
    """
    Форма для создания вагона
    """
    type: Union[int, str]
    settings: Optional[List[WagonSettingCreate]] = []


class WagonCreateDB(BaseWagon):
    type_id: int


class WagonUpdate(BaseWagon):
    """
    Форма для изменения вагона
    """
    pass


class WagonWithoutSettings(BaseWagon):
    id: int
    created_at: datetime
    updated_at: Optional[datetime]
    owner_id: int
    type: WagonTypeShortInfo

    class Config:
        orm_mode = True


class Wagon(WagonWithoutSettings):
    """
    Датаграмма вагона для ответа сервером
    """
    settings: List[WagonSetting]
