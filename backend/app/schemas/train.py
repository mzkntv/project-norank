from typing import List

from pydantic import BaseModel

from app.schemas.wagon import Wagon, WagonWithoutSettings


class BaseTrain(BaseModel):
    name: str
    depot_id: int


class TrainCreate(BaseTrain):
    pass


class TrainUpdate(BaseTrain):
    owner_id: int


class TrainList(BaseTrain):
    id: int
    owner_id: int
    wagons: List[WagonWithoutSettings]

    class Config:
        orm_mode = True


class Train(BaseTrain):
    id: int
    owner_id: int
    wagons: List[Wagon]

    class Config:
        orm_mode = True


class BaseDepot(BaseModel):
    name: str


class DepotCreate(BaseDepot):
    pass


class DepotUpdate(BaseDepot):
    pass


class Depot(BaseDepot):
    id: int

    class Config:
        orm_mode = True
