from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class BaseBan(BaseModel):
    user_id: int


class BanCreate(BaseBan):
    reason: Optional[str] = None
    banned_until: Optional[datetime] = None


class BanUpdate(BaseBan):
    banned_until: Optional[datetime] = None


class BanDelete(BaseBan):
    reason: str


class BaseBanInDB(BaseBan):
    id: int
    created_at: datetime
    updated_at: Optional[datetime]
    reason: Optional[str] = None
    banned_until: Optional[datetime] = None

    class Config:
        orm_mode = True


class Ban(BaseBanInDB):
    created_by: int
    updated_by: Optional[int]


class BanInDB(BaseBanInDB):
    created_by: int
    updated_by: Optional[int]


class BaseJustifiedBan(BaseModel):
    reason: Optional[str] = None
    ban_id: int


class JustifiedBanCreate(BaseJustifiedBan):
    created_by: int


class JustifiedBanUpdate(BaseJustifiedBan):
    pass


class BaseJustifiedBanInDB(BaseJustifiedBan):
    id: int
    created_at: datetime
    updated_at: Optional[datetime] = None

    class Config:
        orm_mode = True


class JustifiedBan(BaseJustifiedBanInDB):
    created_by: int
    updated_by: Optional[int] = None


class JustifiedBanInDB(BaseJustifiedBanInDB):
    created_by: int
    updated_by: Optional[int] = None
