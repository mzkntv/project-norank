from pydantic import BaseModel


class BaseWagonSetting(BaseModel):
    sys: str
    value: str
    type: str
    is_system: bool


class WagonSettingCreate(BaseWagonSetting):
    pass


class WagonSettingUpdate(BaseWagonSetting):
    pass


class WagonSettingCreateDB(BaseWagonSetting):
    wagon_id: int


class WagonSetting(BaseWagonSetting):
    id: int
    wagon_id: str

    class Config:
        orm_mode = True
