from typing import Optional, List

from pydantic import BaseModel

from app.schemas.users.role import RoleWithoutPermissions


class BaseUser(BaseModel):
    email: Optional[str] = 'test@test.ru'
    username: Optional[str] = 'test'


class UserCreate(BaseUser):
    password: str


class UserUpdate(BaseUser):
    pass


class BaseUserInDB(BaseUser):
    id: int
    steam_id: Optional[str] = None
    is_active: bool

    class Config:
        orm_mode = True


class User(BaseUserInDB):
    roles: List[RoleWithoutPermissions]


class UserInDB(BaseUserInDB):
    pass


class Token(BaseModel):
    access_token: str
    token_type: str = 'bearer'


class TokenData(BaseModel):
    username: Optional[str] = None
    scopes: List[str] = []


class UserInfo(BaseModel):
    user_id: int
    username: str = 'remove_this'
