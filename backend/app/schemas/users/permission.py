from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class BasePermission(BaseModel):
    title: str
    module: str
    key: str


class PermissionCreate(BasePermission):
    pass


class PermissionUpdate(BasePermission):
    pass


class BasePermissionInDB(BasePermission):
    id: int
    created_at: datetime
    updated_at: Optional[datetime] = None

    class Config:
        orm_mode = True


class Permission(BasePermissionInDB):
    pass


class PermissionInDB(BasePermissionInDB):
    pass
