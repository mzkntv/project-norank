from datetime import datetime
from typing import Optional, List

from pydantic import BaseModel

from app.schemas.users.permission import Permission


class BaseRole(BaseModel):
    title: str
    parent: Optional[int] = None


class RoleCreate(BaseRole):
    pass


class RoleUpdate(BaseRole):
    pass


class RoleInDBBase(BaseRole):
    id: int
    created_at: datetime
    updated_at: Optional[datetime] = None

    class Config:
        orm_mode = True


class Role(RoleInDBBase):
    """
    Role to return to client
    """
    permissions: List[Permission]


class RoleWithoutPermissions(RoleInDBBase):
    pass


class RoleInDB(RoleInDBBase):
    """
    Role stored in DB
    """
    pass
