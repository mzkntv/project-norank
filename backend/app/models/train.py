
import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.db.base_class import Base


def random_unique_registration_number():
    from app.db.session import Session
    from random import randint

    min_ = 1
    max_ = 99999

    db = Session()

    randomized_number = randint(min_, max_)

    while (db.query(WagonInstance)
            .where(WagonInstance.registration_number == randomized_number)
            .limit(1)
            .first()) is not None:
        randomized_number = randint(min_, max_)

    return randomized_number


class Depot(Base):
    name = sa.Column(sa.String)

    depot_trains = relationship('Train', back_populates='depot')


class Train(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(255), default='Unnamed train')
    owner_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), index=True)
    depot_id = sa.Column(sa.Integer, sa.ForeignKey('depot.id'), index=True)

    owner = relationship('User', back_populates='owned_trains')
    depot = relationship('Depot', back_populates='depot_trains')
    wagons = relationship('WagonInstance', back_populates='train', order_by='WagonInstance.train_position')
    ms_event_members = relationship('MetrostroiEventMember', back_populates='train', cascade='all, delete-orphan')


class WagonType(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String, unique=True)
    metrostroi_representation_class = sa.Column(sa.String, unique=True)
    name = sa.Column(sa.String, unique=True)
    description = sa.Column(sa.String, nullable=True)

    wagons = relationship('WagonInstance', back_populates='type')


class WagonInstance(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    registration_number = sa.Column(sa.Integer, nullable=False, unique=True, default=random_unique_registration_number)
    mileage = sa.Column(sa.Integer, default=0, nullable=False)
    train_position = sa.Column(sa.Integer, nullable=False)
    inverted = sa.Column(sa.Boolean, default=False)
    owner_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), index=True)
    type_id = sa.Column(sa.Integer, sa.ForeignKey('wagontype.id'), nullable=False)
    train_id = sa.Column(sa.Integer, sa.ForeignKey('train.id'))

    owner = relationship('User', back_populates='wagons')
    type = relationship('WagonType', back_populates='wagons')
    train = relationship('Train', back_populates='wagons')
    settings = relationship('WagonSetting', back_populates='wagon', cascade='all, delete-orphan')

    __table_args__ = (
        sa.CheckConstraint(registration_number >= 0, name='check_reg_number_positive'),
        sa.CheckConstraint(mileage > 0, name='check_mileage_positive'),
        sa.UniqueConstraint('registration_number', name='uq_registration_number'),
        sa.UniqueConstraint('train_position', 'train_id', name='_train_position_train_id_uc'),
    )


class WagonSetting(Base):
    sys = sa.Column(sa.String(255), nullable=False)
    value = sa.Column(sa.String(64), nullable=False)
    type = sa.Column(sa.String(64), nullable=False)
    is_system = sa.Column(sa.Boolean, nullable=False)
    wagon_id = sa.Column(sa.ForeignKey('wagoninstance.id'), nullable=False)

    wagon = relationship('WagonInstance', back_populates='settings')

    __table_args__ = (
        sa.UniqueConstraint('sys', 'wagon_id', name='_sys_wagon_id'),
    )
