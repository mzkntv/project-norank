import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.db.base_class import Base


role_permission_relation = sa.Table(
    'role_permission_relation',
    Base.metadata,
    sa.Column('permission_id', sa.ForeignKey('permission.id'), primary_key=True),
    sa.Column('role_id', sa.ForeignKey('role.id'), primary_key=True),
)

role_user_relation = sa.Table(
    'role_user_relation',
    Base.metadata,
    sa.Column('user_id', sa.ForeignKey('user.id'), primary_key=True),
    sa.Column('role_id', sa.ForeignKey('role.id'), primary_key=True),
)


class User(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    email = sa.Column(sa.String, unique=True)
    username = sa.Column(sa.String, unique=True)
    hashed_password = sa.Column(sa.Text)
    is_active = sa.Column(sa.Boolean, default=True)
    steam_id = sa.Column(sa.String(64), unique=True)

    roles = relationship('Role', secondary=role_user_relation, back_populates='users')

    created_events = relationship('EventInstance', back_populates='creator', foreign_keys='EventInstance.created_by')
    updated_events = relationship('EventInstance', back_populates='updator', foreign_keys='EventInstance.updated_by')

    owned_trains = relationship('Train', back_populates='owner')
    wagons = relationship('WagonInstance', back_populates='owner')

    bans = relationship('Ban', back_populates='perpetrator', foreign_keys='Ban.user_id')
    users_banned = relationship('Ban', back_populates='creator', foreign_keys='Ban.created_by')
    users_justified = relationship('JustifiedBan', back_populates='creator')


class Role(Base):
    title = sa.Column(sa.String(64), nullable=False)
    parent = sa.Column(sa.Integer, sa.ForeignKey('role.id'))

    children = relationship('Role')
    users = relationship('User', secondary=role_user_relation, back_populates='roles')
    permissions = relationship('Permission',
                               secondary=role_permission_relation,
                               back_populates='roles',
                               cascade='all, delete')


class Permission(Base):
    title = sa.Column(sa.String(64), nullable=False)
    module = sa.Column(sa.String(64), nullable=False)
    key = sa.Column(sa.String(64), nullable=False)

    roles = relationship('Role', secondary=role_permission_relation, back_populates='permissions')
