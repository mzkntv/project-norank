import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Ban(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), index=True, nullable=False)
    created_by = sa.Column(sa.Integer, sa.ForeignKey('user.id'), index=True, nullable=False)
    updated_by = sa.Column(sa.Integer, sa.ForeignKey('user.id'), index=True)
    reason = sa.Column(sa.String, nullable=True)
    banned_until = sa.Column(sa.DateTime, nullable=True)

    perpetrator = relationship('User', back_populates='bans', foreign_keys=[user_id])
    creator = relationship('User', back_populates='users_banned', foreign_keys=[created_by])
    editor = relationship('User', foreign_keys=[updated_by])
    justified = relationship('JustifiedBan', back_populates='ban', uselist=False)

    __tableargs__ = (
        sa.CheckConstraint('banned_until' > 'created_at', name='check_ban_timedelta_positive'),
        sa.CheckConstraint(created_by != user_id, name='check_self_ban'),
    )


class JustifiedBan(Base):
    created_by = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    reason = sa.Column(sa.String, nullable=True)
    ban_id = sa.Column(sa.Integer, sa.ForeignKey('ban.id'), nullable=False)

    creator = relationship('User', back_populates='users_justified')
    ban = relationship('Ban', back_populates='justified')
