import sqlalchemy as sa
from sqlalchemy import UniqueConstraint, CheckConstraint
from sqlalchemy.orm import relationship, declared_attr, column_property

from app.db.base_class import Base


class BaseEventMember(Base):
    __abstract__ = True

    @declared_attr
    def user_id(cls):
        return sa.Column(sa.Integer, sa.ForeignKey('user.id'), index=True, nullable=False)

    @declared_attr
    def user(cls):
        return relationship('User')

    @declared_attr
    def event_id(cls):
        return sa.Column(sa.Integer, sa.ForeignKey('eventinstance.id'), index=True, nullable=False)

    @declared_attr
    def event(cls):
        return relationship('EventInstance', back_populates='members')


class MetrostroiEventMember(BaseEventMember):
    """
    Отдельная таблица для RP-поездок в метрострое.
    На случай, если появятся еще другие виды мероприятий
    """
    train_id = sa.Column(sa.Integer, sa.ForeignKey('train.id'), index=True, nullable=False)

    train = relationship('Train', back_populates='ms_event_members')

    __mapper_args__ = {
        'polymorphic_identity': 'MetrostroiEventMember',
    }
    __table_args__ = (
        UniqueConstraint('event_id', 'train_id'),
    )


class EventInstance(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    created_by = sa.Column(sa.Integer, sa.ForeignKey('user.id'), index=True, nullable=False)
    updated_by = sa.Column(sa.Integer, sa.ForeignKey('user.id'), index=True)
    name = sa.Column(sa.String)
    description = sa.Column(sa.String, nullable=True)
    start = sa.Column(sa.DateTime, nullable=True)
    finish = sa.Column(sa.DateTime, nullable=True)
    max_number_of_participants = sa.Column(sa.Integer, nullable=True)
    participants_count = column_property(
        sa.select(sa.func.count(MetrostroiEventMember.id)).
        where(MetrostroiEventMember.event_id == id).
        correlate_except(MetrostroiEventMember).
        scalar_subquery()
    )

    creator = relationship('User', back_populates='created_events', foreign_keys=[created_by])
    updator = relationship('User', back_populates='updated_events', foreign_keys=[updated_by])
    members = relationship('MetrostroiEventMember')


    __table_args__ = (
        CheckConstraint(max_number_of_participants > 0, name='check_positive_participants'),
    )
