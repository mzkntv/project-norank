"""ban refactor

Revision ID: 780e35fb422b
Revises: de87224d04cf
Create Date: 2021-12-12 20:13:45.275310

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '780e35fb422b'
down_revision = 'de87224d04cf'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('justifiedban',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_at', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('created_by', sa.Integer(), nullable=False),
    sa.Column('reason', sa.String(), nullable=True),
    sa.Column('ban_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['ban_id'], ['ban.id'], ),
    sa.ForeignKeyConstraint(['created_by'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_constraint('ban_justified_by_fkey', 'ban', type_='foreignkey')
    op.drop_column('ban', 'justified_at')
    op.drop_column('ban', 'justified_by')
    op.drop_column('ban', 'justified')
    op.drop_column('ban', 'justify_reason')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('ban', sa.Column('justify_reason', sa.VARCHAR(), autoincrement=False, nullable=True))
    op.add_column('ban', sa.Column('justified', sa.BOOLEAN(), autoincrement=False, nullable=False))
    op.add_column('ban', sa.Column('justified_by', sa.INTEGER(), autoincrement=False, nullable=True))
    op.add_column('ban', sa.Column('justified_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True))
    op.create_foreign_key('ban_justified_by_fkey', 'ban', 'user', ['justified_by'], ['id'])
    op.drop_table('justifiedban')
    # ### end Alembic commands ###
