"""
Various utility's functions for development & testing
"""
import os
import uuid
from argparse import Namespace
from contextlib import contextmanager
from pathlib import Path
from types import SimpleNamespace
from typing import Optional, Union

from alembic.config import Config
from sqlalchemy_utils import create_database, drop_database
from yarl import URL


PROJECT_PATH = Path(__file__).parent.resolve()


def make_alembic_config(cmd_options: Union[Namespace, SimpleNamespace],
                        base_path: str = PROJECT_PATH) -> Config:
    """
    Generate alembic config to test migrations in test databases
    :param cmd_options: Alembic options
    :param base_path: path to project
    :return: Alembic config
    """
    # Replace path to alembic.ini file to absolute
    if not os.path.isabs(cmd_options.config):
        cmd_options.config = os.path.join(base_path, cmd_options.config)

    config = Config(file_=cmd_options.config, ini_section=cmd_options.name, cmd_opts=cmd_options)

    # Replace path to alembic folder to absolute
    alembic_location = config.get_main_option('script_location')
    if not os.path.isabs(alembic_location):
        config.set_main_option('script_location', os.path.join(base_path, alembic_location))
    if cmd_options.pg_url:
        config.set_main_option('sqlalchemy.url', cmd_options.pg_url)

    return config


def alembic_config_from_url(pg_url: Optional[str] = None) -> Config:
    """
    Provides Python object, representing alembic.ini file.
    """
    cmd_options = SimpleNamespace(
        config='alembic.ini',
        name='alembic',
        pg_url=pg_url,
        raiseerr=False,
        x=None
    )

    return make_alembic_config(cmd_options)


@contextmanager
def tmp_database(db_url: URL, suffix: str = '', **kwargs):
    """Creates temporary database to run migration test"""
    tmp_db_name = '.'.join([uuid.uuid4().hex, 'norank', suffix])
    tmp_db_url = str(db_url.with_path(tmp_db_name))
    create_database(tmp_db_url, **kwargs)

    try:
        yield tmp_db_url
    finally:
        drop_database(tmp_db_url)
