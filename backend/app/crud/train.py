from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models import Train, MetrostroiEventMember, Depot
from app.schemas.train import TrainCreate, TrainUpdate, DepotCreate, DepotUpdate


class CRUDTrain(CRUDBase[Train, TrainCreate, TrainUpdate]):
    def get_many_trains_not_in_event(self, db: Session, event_id: int):
        """
        Получаем список всех поездов, которые не участвуют в мероприятии
        """
        return (
            db.query(self.model).where(self.model.id.not_in(
                db.query(MetrostroiEventMember.train_id).where(MetrostroiEventMember.event_id == event_id)
            )).all()
        )

    def create_with_author(self, db: Session, obj_in: TrainCreate, author_id: int):
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=author_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)

        return db_obj

    def get_trains_by_owner(self, db: Session, user_id: int) -> List[Train]:
        """
        Получить список поездов, владельцем которых является user_id
        :param db: сессия
        :param user_id: ID пользователя
        :return: список поездов
        """
        return db.query(self.model).where(self.model.owner_id == user_id).all()

    def get_train_by_owner(self, db: Session, train_id: int, user_id: int) -> Optional[Train]:
        """
        Получить объект состава из БД, если пользователь является владельцем состава
        :param db: сессия
        :param train_id: ID состава
        :param user_id: ID пользователя
        :return: optional: объект состава из бд
        """
        return (
            db.query(self.model)
            .where((self.model.owner_id == user_id) & (self.model.id == train_id))
            .first()
        )


class CRUDDepot(CRUDBase[Depot, DepotCreate, DepotUpdate]):
    pass


train = CRUDTrain(Train)
depot = CRUDDepot(Depot)
