"""
CRUD operations with Ban table
"""
from typing import Union, Optional, List

from fastapi.encoders import jsonable_encoder
from sqlalchemy import func
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models import Ban, JustifiedBan
from app.schemas.ban import BanCreate, BanUpdate, BanDelete, JustifiedBanCreate, JustifiedBanUpdate


class CRUDBan(CRUDBase[Ban, BanCreate, BanUpdate]):
    def get_many_by_perpetrator_id(self, db: Session, perpetrator_id: int) -> List[Ban]:
        return db.query(self.model).where(self.model.user_id == perpetrator_id).all()

    def create_with_author(self, db: Session, obj_in: BanCreate, author_id: int) -> Ban:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, created_by=author_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)

        return db_obj

    def update_with_editor(
            self,
            db: Session,
            db_obj: Ban,
            obj_in: Union[BanUpdate, dict],
            editor_id: int
    ):
        if isinstance(obj_in, dict):
            update_data = dict(**obj_in, updated_by=editor_id)
        else:
            update_data = dict(
                **obj_in.dict(exclude_unset=True),
                updated_by=editor_id,
            )
        return self.update(db, db_obj, update_data)

    def get_active_ban_by_user_id(self, db: Session, user_id: int) -> Optional[Ban]:
        """Returns an id of active ban record from DB if it exists"""
        return (
            db.query(self.model)
            .where(
                self.model.user_id == user_id,
                self.model.banned_until > func.now(),
                self.model.justified == None,
            )
            .first()
        )


class CRUDJustifiedBan(CRUDBase[JustifiedBan, JustifiedBanCreate, JustifiedBanUpdate]):
    pass


ban = CRUDBan(Ban)
justified_ban = CRUDJustifiedBan(JustifiedBan)
