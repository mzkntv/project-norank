from typing import Optional, List

from sqlalchemy.orm import Session

from app.models import User, Role, Permission
from app.schemas.auth import UserCreate, UserUpdate
from .base import CRUDBase
from ..schemas.users.role import RoleCreate, RoleUpdate


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    def create(self, db: Session, user_data: UserCreate) -> User:
        _user = User(
            email=user_data.email,
            username=user_data.username,
            hashed_password=user_data.password
        )
        db.add(_user)
        db.commit()

        return _user

    def get_by_username(self, db: Session, username: str) -> User:
        return db.query(User).filter(User.username == username).first()

    def get_by_steam_id(self, db: Session, steam_id: str) -> Optional[User]:
        return db.query(User).filter(User.steam_id == steam_id).first()

    def create_by_steam_id(self, db: Session, steam_id: str) -> User:
        """
        Создать пользователя через Steam
        :param db: сессия бд
        :param steam_id: SteamID64 пользователя
        :return: SQLAlchemy's model User
        """
        _user = User(
            steam_id=steam_id,
        )
        db.add(_user)
        db.commit()

        return _user

    def get_user_roles(self, db: Session, user_id: int) -> List[Role]:
        return db.query(Role).join(self.model.roles).where(self.model.id == user_id).all()

    def get_user_permissions(self, db: Session, user_id: int) -> List[Permission]:
        topq = db.query(Role.id, Role.parent).join(self.model.roles).where(self.model.id == user_id)
        topq = topq.cte('cte', recursive=True)
        bottomq = db.query(Role.id, Role.parent).join(topq, Role.id == topq.c.parent)
        recursive_q = topq.union(bottomq)

        return (
            db.query(Permission)
            .select_from(recursive_q)
            .join(Role.permissions)
            .all()
        )


class CRUDRole(CRUDBase[Role, RoleCreate, RoleUpdate]):
    pass


user = CRUDUser(User)
role = CRUDRole(Role)
