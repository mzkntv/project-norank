from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud import CRUDBase
from app.models import WagonType, WagonInstance, WagonSetting
from app.schemas.wagon import BaseWagonType, WagonUpdate, WagonCreateDB
from app.schemas.wagons.settings import WagonSettingUpdate, WagonSettingCreateDB, WagonSettingCreate


class CRUDWagonType(CRUDBase[WagonType, BaseWagonType, BaseWagonType]):
    def get_by_metrostroi_representation_class(self, db: Session, metrostroi_class: str) -> WagonType:
        return db.query(self.model.id).where(self.model.metrostroi_representation_class == metrostroi_class).first()


class CRUDWagon(CRUDBase[WagonInstance, WagonCreateDB, WagonUpdate]):
    def get_by_registration_number(self, db: Session, registration_number: int):
        return db.query(self.model.id).where(self.model.registration_number == registration_number).first()

    def create_with_owner(self, db: Session, owner_id: int, wagon_in: WagonCreateDB) -> WagonInstance:
        wagon_in_data = jsonable_encoder(wagon_in)
        wagon_db = self.model(**wagon_in_data, owner_id=owner_id)
        db.add(wagon_db)
        db.commit()
        db.refresh(wagon_db)
        return wagon_db


class CRUDWagonSettings(CRUDBase[WagonSetting, WagonSettingCreateDB, WagonSettingUpdate]):
    def get_by_wagon(self, db: Session, wagon_id: int):
        return db.query(self.model).where(self.model.wagon_id == wagon_id).all()

    def create(self, wagon_id: int, obj_in: WagonSettingCreate, *args, **kwargs) -> WagonSetting:
        _obj_in = WagonSettingCreateDB(**obj_in.dict(), wagon_id=wagon_id)
        return super(CRUDWagonSettings, self).create(obj_in=_obj_in, *args, **kwargs)


wagon_type = CRUDWagonType(WagonType)
wagon = CRUDWagon(WagonInstance)
wagon_setting = CRUDWagonSettings(WagonSetting)
