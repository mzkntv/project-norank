from typing import List, Optional

from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy import func
from sqlalchemy.orm import Session
from starlette import status

from app.crud.train import train
from app.crud.base import CRUDBase
from app.models import EventInstance, MetrostroiEventMember, User
from app.schemas.auth import UserInfo
from app.schemas.event import (
    EventInstance as EventInstanceSchema,
    EventInstanceCreate,
    EventInstanceUpdate,
    EventMemberCreate,
    EventMemberUpdate,
    EventInstanceWithUserInfo,
)


class CRUDEventInstance(CRUDBase[EventInstance, EventInstanceCreate, EventInstanceUpdate]):
    def get(self, db: Session, id_: int) -> Optional[EventInstanceWithUserInfo]:
        event_instance_model: EventInstance = super(CRUDEventInstance, self).get(db, id_)
        event_instance_schema = EventInstanceSchema.from_orm(event_instance_model)
        event_instance_schema_with_creator = EventInstanceWithUserInfo(
            **event_instance_schema.dict(),
            created_by=UserInfo(user_id=id_, username='teeeeest'),
        )
        return event_instance_schema_with_creator

    def get_members(self, db: Session, id_: int):
        return db.query(User).join(self.model.members, MetrostroiEventMember.user).where(self.model.id == id_).all()

    def get_many(self, db: Session) -> List[EventInstanceWithUserInfo]:
        event_instance_schemas = []
        for event_instance in super(CRUDEventInstance, self).get_many(db):
            event_instance_schemas.append(
                EventInstanceWithUserInfo(
                    **EventInstanceSchema.from_orm(event_instance).dict(),
                    created_by=UserInfo(user_id=222, username='teeest'),
                )
            )
        return event_instance_schemas

    def get_by_author(self, db: Session, author_id: int) -> List[EventInstance]:
        return db.query(self.model).where(self.model.created_by == author_id).all()

    def create_with_author(self, db: Session, obj_in: EventInstanceCreate, author_id: int) -> EventInstance:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, created_by=author_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)

        return db_obj

    def update_with_editor(
            self,
            db: Session,
            db_obj: EventInstance,
            obj_in: EventInstanceUpdate,
            editor_id: int
    ) -> EventInstance:
        update_data: dict = dict(
            **obj_in.dict(exclude_unset=True),
            updated_by=editor_id
        )
        return self.update(db, db_obj, update_data)


class CRUDEventMember(CRUDBase[MetrostroiEventMember, EventMemberCreate, EventMemberUpdate]):
    def get_by_event_and_user(self, db: Session, event_id: int, user_id: int) -> Optional[MetrostroiEventMember]:
        _event = event.get(db, event_id)
        if not _event:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Event not found',
            )

        return db.query(self.model).where(self.model.event_id == event_id, self.model.user_id == user_id).first()

    def get_by_event_and_train(self, db: Session, event_id: int, train_id: int) -> Optional[MetrostroiEventMember]:
        _event = event.get(db, event_id)
        _train = train.get(db, train_id)

        if not _event or not _train:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Unable to find event or train'
            )

        return db.query(self.model).where(self.model.event_id == event_id, self.model.train_id == train_id).first()

    def count_event_members(self, db: Session, event_id: int):
        return db.query(func.count(self.model.id)).where(self.model.event_id == event_id)

    def get_participants_list(self, db: Session, event_id: int):
        return db.query(User).join(self.model).where(self.model.event_id == event_id).all()


event = CRUDEventInstance(EventInstance)
event_member = CRUDEventMember(MetrostroiEventMember)
