"""
Основной модуль запуска приложения FastAPI
"""
from fastapi import FastAPI

from app.api import router

app = FastAPI(
    description='RESTful API для norank.ru',
    version='1.0.0'
)

app.include_router(
    router,
)


@app.get('/ping')
def ping():
    """Health-check"""
    return {'ping': 'pong'}
