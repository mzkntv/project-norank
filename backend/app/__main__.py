"""
Module helps integrate PyCharm with FastAPI
"""
import uvicorn

from .settings import settings


uvicorn.run(
    'backend.app.main:app',
    host=settings.server_host,
    port=settings.server_port,
    root_path='/api',
    reload=True,
)
