from typing import List

from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session

from app import crud
from app.api.dependencies import get_session
from app.models import EventInstance, BaseEventMember
from app.schemas.event import (
    EventInstanceCreate, EventInstanceUpdate, MetrostroiEventMemberCreate,
    EventInstanceWithUserInfo
)


class EventInstanceService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get(self, event_id: int, user_id: int) -> EventInstanceWithUserInfo:
        """
        Получить информацию о мероприятии
        """
        event_db: EventInstance = crud.event.get(self.session, event_id)

        event = EventInstanceWithUserInfo.from_orm(event_db)

        for event_member in crud.event.get_members(self.session, event_id):
            if event_member.id == user_id:
                event.is_registered = True

        if not event_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Event not found',
            )

        return event

    def get_many(self) -> List[EventInstance]:
        """
        Получить список мероприятий
        """
        return crud.event.get_many(self.session)

    def create(self, event_data: EventInstanceCreate, author_id: int) -> EventInstance:
        """
        Создать мероприятие
        """
        return crud.event.create_with_author(self.session, event_data, author_id)

    def update(self, event_id: int, editor_id: int, event_data: EventInstanceUpdate):
        """
        Обновить информацию о мероприятии
        """
        event = crud.event.get(self.session, event_id)
        if not event:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Event not found',
            )
        return crud.event.update_with_editor(self.session, event, event_data, editor_id)

    def delete(self, event_id: int, user_id: int):
        """
        Удалить мероприятие
        """
        event = crud.event.get(self.session, event_id)
        if not event:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Event not found',
            )
        return crud.event.remove(self.session, event)

    def subscribe_user_to_event(self, user_id: int, event_id: int, train_id: int) -> BaseEventMember:
        """
        Регистрация пользователя на мероприятие
        """
        membership: BaseEventMember = crud.event_member.get_by_event_and_user(self.session, event_id, user_id)
        if membership:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Could not register user to event - already registered'
            )

        already_booked = crud.event_member.get_by_event_and_train(self.session, event_id, train_id)
        if already_booked:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Не удалось забронировать состав - он уже забронирован кем-то',
            )

        membership = crud.event_member.create(
            self.session,
            MetrostroiEventMemberCreate(user_id=user_id, event_id=event_id, train_id=train_id)
        )
        return membership

    def unsubscribe_user_from_event(self, user_id: int, event_id: int):
        """
        Отмена регистрации на мероприятие
        """
        membership: BaseEventMember = crud.event_member.get_by_event_and_user(self.session, event_id, user_id)

        if not membership:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Could not check user's membership in the event"
            )

        return crud.event_member.remove(self.session, membership)

    def get_list_of_participants(self, event_id: int):
        return crud.event_member.get_participants_list(self.session, event_id)
