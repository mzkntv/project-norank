from typing import List, Union

from fastapi import Depends, HTTPException
from starlette import status

from app import crud
from app.api.dependencies import get_session
from app.db.session import Session
from app.models import WagonType, WagonInstance
from app.schemas.wagon import BaseWagonType, Wagon, WagonCreate, WagonCreateDB
from app.schemas.wagons.settings import WagonSettingCreate


class WagonService:
    """
    Сервис управления вагонами
    """
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_all_types(self) -> List[WagonType]:
        return crud.wagon_type.get_many(self.session)

    def get_wagon_type(self, type_id: int) -> WagonType:
        wagon_type_db = crud.wagon_type.get(self.session, type_id)

        if not wagon_type_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Could not find wagon type with this id',
            )

        return wagon_type_db

    def create_wagon_type(self, wagon_data: BaseWagonType) -> WagonType:
        return crud.wagon_type.create(self.session, wagon_data)

    def get_all_wagons(self) -> List[Wagon]:
        return crud.wagon.get_many(self.session)

    def get_wagon(self, wagon_id: int) -> Wagon:
        wagon_db = crud.wagon.get(self.session, wagon_id)

        if not wagon_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Could not find wagon with this id',
            )

        return crud.wagon.get(self.session, wagon_id)

    def _get_wagon_type_id(self, type_: Union[int, str]) -> int:
        if isinstance(type_, int):
            return type_
        type_db = crud.wagon_type.get_by_metrostroi_representation_class(self.session, type_)
        if not type_db:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Could not find this wagon type',
            )

        return type_db.id

    def create_wagon(self, owner_id: int, wagon_data: WagonCreate) -> WagonInstance:
        wagon = crud.wagon.get_by_registration_number(self.session, wagon_data.registration_number)
        if wagon:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Wagon with this registration number already exists',
            )

        wagon_to_db_data = WagonCreateDB(
            **wagon_data.dict(exclude={'type', 'settings'}),
            type_id=self._get_wagon_type_id(wagon_data.type),
        )

        wagon_db = crud.wagon.create_with_owner(self.session, owner_id, wagon_to_db_data)

        self.add_wagon_settings(wagon_db.id, wagon_data.settings)

        return wagon_db

    def create_wagons(self, owner_id: int, wagon_data_list: List[WagonCreate]) -> List[WagonInstance]:
        return [
            self.create_wagon(owner_id, wagon_data)
            for wagon_data in wagon_data_list
        ]

    def get_wagon_settings(self, wagon_id: int):
        wagon_db = crud.wagon.get(self.session, wagon_id)
        if not wagon_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Could not find wagon with this id',
            )

        return crud.wagon_setting.get_by_wagon(self.session, wagon_id)

    def add_wagon_settings(self, wagon_id: int, settings_list: List[WagonSettingCreate]):
        wagon_db = crud.wagon.get(self.session, wagon_id)
        if not wagon_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Could not find wagon with this id',
            )

        wagon_settings_list_db = [
            crud.wagon_setting.create(db=self.session, obj_in=setting, wagon_id=wagon_id)
            for setting in settings_list
        ]

        return wagon_settings_list_db

