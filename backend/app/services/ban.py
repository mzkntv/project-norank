"""
Сервис учета блокировок пользователей
"""
from typing import List

from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from app import crud
from app.api.dependencies import get_session
from app.models import Ban
from app.schemas.ban import BanCreate, JustifiedBanCreate, BanDelete


class BanService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_ban_history(self, user_id: int) -> List[Ban]:
        """
        Получить историю банов игрока
        """
        return crud.ban.get_many_by_perpetrator_id(self.session, user_id)

    def get_many(self) -> List[Ban]:
        return crud.ban.get_many(self.session)

    def ban_user(self, perpetrator_data: BanCreate, user_id: int) -> Ban:
        """
        Забанить игрока
        """
        # Проверка на идиота - не даем забанить самого себя
        if user_id == perpetrator_data.user_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Self bans are restricted',
            )

        user = crud.user.get(self.session, perpetrator_data.user_id)
        if not user:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='User not found',
            )

        # Проверяем, что пользователь не имеет активных банов
        active_ban = crud.ban.get_active_ban_by_user_id(self.session, perpetrator_data.user_id)
        if active_ban:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='User has an active ban'
            )

        return crud.ban.create_with_author(self.session, perpetrator_data, user_id)

    def unban_user(self, ban_data: BanDelete, initiator_id: int):
        """
        Разбанить игрока
        :param ban_data: валидированные данные причины бана и ид нарушителя
        :param initiator_id: id администратора, который разбанить
        """
        perpetrator = crud.user.get(self.session, ban_data.user_id)
        if not perpetrator:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='User not found'
            )

        active_ban: Ban = crud.ban.get_active_ban_by_user_id(self.session, ban_data.user_id)
        if not active_ban:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Could not find any active bans for this user'
            )

        return crud.justified_ban.create(
            self.session,
            JustifiedBanCreate(
                reason=ban_data.reason,
                ban_id=active_ban.id,
                created_by=initiator_id,
            )
        )
