"""
Логика управления составами и вагонами
"""
from typing import List

from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from app import crud
from app.api.dependencies import get_session
from app.models import Train, Depot
from app.schemas.train import TrainCreate, DepotCreate


class TrainService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_available_trains(self, event_id: int):
        return crud.train.get_many_trains_not_in_event(self.session, event_id)

    def get_trains_by_owner(self, owner_id: int):
        return crud.train.get_trains_by_owner(self.session, owner_id)

    def get_all_trains(self) -> List[Train]:
        return crud.train.get_many(self.session)

    def get_train(self, train_id) -> Train:
        train_db = crud.train.get(self.session, train_id)

        if not train_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Train not found',
            )

        return train_db

    def create_train(self, train_data: TrainCreate, user_id: int) -> Train:
        depot_db = crud.depot.get(self.session, train_data.depot_id)
        if not depot_db:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Could not find depot',
            )

        return crud.train.create_with_author(self.session, train_data, user_id)

    def delete_train_by_owner(self, train_id: int, user_id: int) -> Train:
        """
        Удалить созданный пользователем состав
        :param train_id: ID состава
        :param user_id: ID пользователя
        :return: объект состава из БД
        """
        train_db = crud.train.get_train_by_owner(self.session, train_id, user_id)

        if not train_db:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Could not find train with this id that you own',
            )

        return crud.train.remove(self.session, train_db)


class DepotService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_many(self):
        return crud.depot.get_many(self.session)

    def get(self, depot_id: int):
        depot_db = crud.depot.get(self.session, depot_id)
        if not depot_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Could not find depot',
            )

        return depot_db

    def create_depot(self, depot_data: DepotCreate, user_id: int) -> Depot:
        return crud.depot.create(self.session, depot_data)
