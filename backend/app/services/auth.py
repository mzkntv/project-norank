import re
from urllib import request
from datetime import datetime, timedelta
from typing import Optional
from urllib.parse import urlencode

from fastapi import HTTPException, status, Depends, Security
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from jose import jwt, JWTError
from passlib.hash import bcrypt
from pydantic import ValidationError
from sqlalchemy.orm import Session
from starlette.datastructures import QueryParams

from app import models, crud
from app.api.dependencies import get_session
from app.schemas.auth import User, Token, UserCreate, UserInDB
from app.settings import settings


def initialize_existing_permissions_from_db():
    from app.db.session import Session as SessionFactory
    from app.models import Permission

    session: Session = SessionFactory()
    scopes = {}

    for permission in session.query(Permission).all():
        scopes.update({
            '{0}:{1}'.format(permission.module, permission.key): permission.title,
        })

    session.close()
    return scopes


oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl='auth/sign-in',
    scopes=initialize_existing_permissions_from_db(),
)


def get_current_user(
        security_scopes: SecurityScopes,
        token: str = Depends(oauth2_scheme),
) -> User:
    """Dependency to current user"""
    return AuthService.validate_token(token, security_scopes)


def get_current_active_user(
        current_user: User = Security(get_current_user)
):
    if not current_user.is_active:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Inactive user',
        )

    return current_user


class AuthService:
    """
    Authentication service
    """

    @classmethod
    def verify_password(cls, plain_password: str, hashed_password: str) -> bool:
        return bcrypt.verify(plain_password, hashed_password)

    @classmethod
    def hash_password(cls, plain_password: str) -> str:
        return bcrypt.hash(plain_password)

    @classmethod
    def validate_token(cls, token: str, security_scopes: SecurityScopes) -> UserInDB:
        if security_scopes.scopes:
            authenticate_value = 'Bearer scope="{0}"'.format(security_scopes.scope_str)
        else:
            authenticate_value = 'Bearer'

        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Could not validate credentials',
            headers={
                'WWW-Authenticate': authenticate_value
            },
        )

        try:
            payload = jwt.decode(
                token,
                settings.jwt_secret,
                algorithms=[settings.jwt_algorithm]
            )
            token_scopes = payload.get('scopes', [])
        except JWTError:
            raise exception from None

        # Проверка на наличие прав доступа к эндпоинту
        for scope in security_scopes.scopes:
            if scope not in token_scopes:
                raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail='Not enough permissions',
                    headers={
                        'WWW-Authenticate': authenticate_value,
                    },
                )

        user_data = payload.get('user')

        try:
            user = UserInDB.parse_obj(user_data)
        except ValidationError:
            raise exception from None

        return user

    def create_token(self, user: models.User) -> Token:
        user_data = UserInDB.from_orm(user)

        now = datetime.utcnow()
        payload = {
            'iat': now,
            'nbf': now,
            'exp': now + timedelta(seconds=settings.jwt_expires_sec),
            'sub': str(user_data.id),
            'user': user_data.dict(),
            'scopes': [
                '{0}:{1}'.format(permission.module, permission.key)
                for permission in crud.user.get_user_permissions(self.session, user_data.id)
            ],
        }
        token = jwt.encode(
            payload,
            settings.jwt_secret,
            algorithm=settings.jwt_algorithm,
        )

        return Token(access_token=token)

    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def register_new_user(self, user_data: UserCreate) -> Token:
        user_data.password = self.hash_password(user_data.password)
        user_db: User = crud.user.create(self.session, user_data)

        return self.create_token(user_db)

    def authenticate_user(self, username: str, password: str) -> Token:
        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={
                'WWW-Authenticate': 'Bearer',
            },
        )

        user = crud.user.get_by_username(self.session, username)

        if not user or not self.verify_password(password, user.hashed_password):
            raise exception

        return self.create_token(user)


class SteamAuthService(AuthService):
    """
    Authentication via Steam provider
    """
    _provider_url: str = 'https://steamcommunity.com/openid/login'

    def validate_nonce(self, results: QueryParams) -> Optional[str]:
        """Parse response's query params and check authenticity with Steam provider"""
        # TODO: нужно переписать всё это с использованием requests
        validation_args = {
            'openid.assoc_handle': results['openid.assoc_handle'],
            'openid.signed': results['openid.signed'],
            'openid.sig': results['openid.sig'],
            'openid.ns': results['openid.ns']
        }

        # Basically, we split apart one of the args steam sends back only to send it back to them to validate!
        # We also append check_authentication which tells OpenID2 to actually know, validate what we send.
        signed_args = results['openid.signed'].split(',')

        for item in signed_args:
            item_arg = 'openid.{0}'.format(item)
            if results[item_arg] not in validation_args:
                validation_args[item_arg] = results[item_arg]

        validation_args['openid.mode'] = 'check_authentication'
        parsed_args = urlencode(validation_args).encode('utf-8')

        with request.urlopen(self._provider_url, parsed_args) as requestData:
            response_data = requestData.read().decode('utf-8')

        # is_valid:true is what Steam returns if something is valid.
        # The alternative is is_valid:false which obviously, is false.
        matched64_id = re.search('https://steamcommunity.com/openid/id/(\d+)', results['openid.claimed_id'])

        if not re.search('is_valid:true', response_data) or not matched64_id:
            return

        return matched64_id.group(1)

    def register_new_user(self, query_params) -> Token:
        """
        Регистрация пользователя через Steam
        :param query_params: параметры, полученные от Steam Provider, после аутентификации в стиме
        :return: Токен для доступа к эндпоинтам
        """
        user_steam_id: str = self.validate_nonce(query_params)

        if not user_steam_id:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='Unable to authorize via steam'
            )

        if crud.user.get_by_steam_id(self.session, user_steam_id):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Could not register via steam - already registered'
            )

        user_db: User = crud.user.create_by_steam_id(self.session, user_steam_id)

        return self.create_token(user_db)

    def authenticate_user(self, query_params: QueryParams, *args, **kwargs) -> Token:
        """
        Check sign provided from Steam, create user if needed and return access token
        """
        user_steam_id: str = self.validate_nonce(query_params)

        if not user_steam_id:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='Unable to authorize via steam'
            )

        user: models.User = crud.user.get_by_steam_id(self.session, user_steam_id)

        if not user:
            user = crud.user.create_by_steam_id(self.session, user_steam_id)

        return self.create_token(user)

    def authenticate_via_gaming_server_token(self, client_steam_id: str) -> Token:
        """
        Авторизация игрока через игровой сервер, чтобы лишний раз не заставлять игрока вводить пароль
        на сервере
        :param client_steam_id: Steam ID игрока на сервере в формате 64
        :return: Токен для доступа к API для клиента
        """

        user_db: models.User = crud.user.get_by_steam_id(self.session, client_steam_id)

        if not user_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Could not find user with this steam id',
            )

        return self.create_token(user_db)
