"""
User management business logic
"""
from typing import List

from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from app import crud
from app.api.dependencies import get_session
from app.models import User, Role, Permission
from app.schemas.users.role import RoleCreate


class UserService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_user_by_user_id(self, user_id: int) -> User:
        user_db = crud.user.get(self.session, user_id)

        if not user_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='User not found',
            )

        return user_db

    def get_list_of_all_roles(self) -> List[Role]:
        return crud.role.get_many(self.session)

    def create_new_role(self, role_data: RoleCreate) -> Role:
        return crud.role.create(self.session, role_data)

    def get_list_of_all_users(self) -> List[User]:
        """
        Get list of all users
        :return: List of SQLAlchemy's user classes
        """
        return crud.user.get_many(self.session)

    def get_user_info_by_user_id(self, user_id: int) -> User:
        user_db = crud.user.get(self.session, user_id)

        if not user_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='User not found',
            )

        return user_db

    def get_user_roles(self, user_id: int) -> List[Role]:
        user_db = crud.user.get(self.session, user_id)

        if not user_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='User not found',
            )

        return crud.user.get_user_roles(self.session, user_id)

    def get_user_permissions(self, user_id: int) -> List[Permission]:
        user_db = crud.user.get(self.session, user_id)

        if not user_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='User not found',
            )

        return crud.user.get_user_permissions(self.session, user_id)

    def get_role_info(self, role_id: int) -> Role:
        role_db = crud.role.get(self.session, role_id)
        if not role_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='Role not found',
            )

        return role_db
