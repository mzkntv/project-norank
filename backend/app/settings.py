"""
Main settings module of the project. All settings should be here.
"""
from pydantic import BaseSettings


class Settings(BaseSettings):
    """
    Pydantic based class. Can use environment variables to create settings
    """
    server_host: str = '127.0.0.1'
    server_port: int = 8008

    database_url: str

    jwt_secret: str
    jwt_algorithm: str = 'HS256'
    jwt_expires_sec: int = 86400

    steam_api_key: str
    steam_redirect_url: str = 'http://localhost:4000/api/auth/sign-up/steam/proceed'


settings = Settings(
    _env_file='.env.local',
    _env_file_encoding='utf-8'
)
